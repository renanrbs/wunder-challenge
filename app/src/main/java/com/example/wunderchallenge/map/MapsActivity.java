package com.example.wunderchallenge.map;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.ViewModelProvider;

import com.example.wunderchallenge.R;
import com.example.wunderchallenge.details.CarDetailsActivity;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;

import java.util.List;

import io.reactivex.disposables.Disposable;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback,
        GoogleMap.OnMyLocationButtonClickListener {

    private static final int LOCATION_PERMISSION_REQUEST_CODE = 1;

    protected GoogleMap mMap;
    protected boolean mPermissionDenied = false;
    private Disposable disposable;

    MapsViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        if (mapFragment != null) {
            mapFragment.getMapAsync(this);
        }
        viewModel = new ViewModelProvider(this).get(MapsViewModel.class);
    }

    @Override
    protected void onDestroy() {
        if (disposable != null) disposable.dispose();
        super.onDestroy();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setOnMyLocationButtonClickListener(this);

        enableMyLocation();
        goToCurrentLocation();

        disposable = viewModel.getCarLocations()
                .subscribe(this::setCarMarkers,
                        error -> Toast.makeText(this, getString(R.string.default_error),
                                Toast.LENGTH_SHORT).show());
    }

    private void goToCurrentLocation() {
        LocationManager manager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        String bestProvider = String.valueOf(manager.getBestProvider(new Criteria(), true));

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            Location mLocation = manager.getLastKnownLocation(bestProvider);
            if (mLocation != null) {
                mMap.moveCamera(CameraUpdateFactory.newLatLng(
                        new LatLng(mLocation.getLatitude(), mLocation.getLongitude())));
                mMap.moveCamera(CameraUpdateFactory.zoomTo(15));
            }
        }
    }

    @Override
    public boolean onMyLocationButtonClick() {
        return false;
    }

    private void setCarMarkers(List<CarLocationDTO> dtoList) {
        for (CarLocationDTO location : dtoList) {
            Marker marker = mMap.addMarker(location.getMarkerOptions());
            location.setMarker(marker);
        }

        mMap.setOnMarkerClickListener(currentMarker -> {
            if (viewModel.isMarkerClicked(currentMarker)) {
                Long id = viewModel.findCurrentCarId(dtoList, currentMarker);
                if (id != null) goToDetails(id);
            } else {
                viewModel.handleMarkerFirstClick(dtoList,
                        currentMarker,
                        getString(R.string.car_title_placeholder));
            }
            return false;
        });

        mMap.setOnMapClickListener(latLng -> {
            for (CarLocationDTO dto : dtoList) {
                dto.getMarker().setVisible(true);
                dto.getMarker().setTitle(null);
            }
        });
    }

    private void enableMyLocation() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            MapPermissionUtils.requestPermission(this, LOCATION_PERMISSION_REQUEST_CODE,
                    Manifest.permission.ACCESS_FINE_LOCATION, true);
        } else if (mMap != null) {
            mMap.setMyLocationEnabled(true);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode != LOCATION_PERMISSION_REQUEST_CODE) {
            return;
        }

        if (MapPermissionUtils.isPermissionGranted(permissions, grantResults,
                Manifest.permission.ACCESS_FINE_LOCATION)) {
            enableMyLocation();
        } else {
            mPermissionDenied = true;
        }
    }

    @Override
    protected void onResumeFragments() {
        super.onResumeFragments();
        if (mPermissionDenied) {
            showMissingPermissionError();
            mPermissionDenied = false;
        }
    }

    private void showMissingPermissionError() {
        MapPermissionUtils.PermissionDeniedDialog
                .newInstance(true).show(getSupportFragmentManager(), "dialog");
    }

    private void goToDetails(long carId) {
        Intent intent = new Intent(this, CarDetailsActivity.class);
        intent.putExtra(CarDetailsActivity.EXTRAR_CAR_ID, carId);
        startActivity(intent);
    }
}
