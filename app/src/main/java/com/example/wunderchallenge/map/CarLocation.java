package com.example.wunderchallenge.map;

class CarLocation {

    private long carId;
    private String title;
    private Double lat;
    private Double lon;
    private String licencePlate;
    private int fuelLevel;
    private long vehicleStateId;
    private long vehicleTypeId;
    private String pricingTime;
    private String pricingParking;
    private int reservationState;
    private boolean isClean;
    private boolean isDamaged;
    private String distance;
    private String address;
    private String zipCode;
    private String city;
    private long locationId;

    public CarLocation(long carId, String title, Double lat, Double lon, String licencePlate, int fuelLevel, long vehicleStateId, long vehicleTypeId, String pricingTime, String pricingParking, int reservationState, boolean isClean, boolean isDamaged, String distance, String address, String zipCode, String city, long locationId) {
        this.carId = carId;
        this.title = title;
        this.lat = lat;
        this.lon = lon;
        this.licencePlate = licencePlate;
        this.fuelLevel = fuelLevel;
        this.vehicleStateId = vehicleStateId;
        this.vehicleTypeId = vehicleTypeId;
        this.pricingTime = pricingTime;
        this.pricingParking = pricingParking;
        this.reservationState = reservationState;
        this.isClean = isClean;
        this.isDamaged = isDamaged;
        this.distance = distance;
        this.address = address;
        this.zipCode = zipCode;
        this.city = city;
        this.locationId = locationId;
    }

    public CarLocation() {
    }

    public long getCarId() {
        return carId;
    }

    public void setCarId(long carId) {
        this.carId = carId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLon() {
        return lon;
    }

    public void setLon(Double lon) {
        this.lon = lon;
    }

    public String getLicencePlate() {
        return licencePlate;
    }

    public void setLicencePlate(String licencePlate) {
        this.licencePlate = licencePlate;
    }

    public int getFuelLevel() {
        return fuelLevel;
    }

    public void setFuelLevel(int fuelLevel) {
        this.fuelLevel = fuelLevel;
    }

    public long getVehicleStateId() {
        return vehicleStateId;
    }

    public void setVehicleStateId(long vehicleStateId) {
        this.vehicleStateId = vehicleStateId;
    }

    public long getVehicleTypeId() {
        return vehicleTypeId;
    }

    public void setVehicleTypeId(long vehicleTypeId) {
        this.vehicleTypeId = vehicleTypeId;
    }

    public String getPricingTime() {
        return pricingTime;
    }

    public void setPricingTime(String pricingTime) {
        this.pricingTime = pricingTime;
    }

    public String getPricingParking() {
        return pricingParking;
    }

    public void setPricingParking(String pricingParking) {
        this.pricingParking = pricingParking;
    }

    public int getReservationState() {
        return reservationState;
    }

    public void setReservationState(int reservationState) {
        this.reservationState = reservationState;
    }

    public boolean isClean() {
        return isClean;
    }

    public void setClean(boolean clean) {
        isClean = clean;
    }

    public boolean isDamaged() {
        return isDamaged;
    }

    public void setDamaged(boolean damaged) {
        isDamaged = damaged;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public long getLocationId() {
        return locationId;
    }

    public void setLocationId(long locationId) {
        this.locationId = locationId;
    }
}
