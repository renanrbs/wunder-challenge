package com.example.wunderchallenge.map;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.GET;

interface CarLocationsService {

    @GET("cars.json")
    Observable<List<CarLocation>> getLocations();

}
