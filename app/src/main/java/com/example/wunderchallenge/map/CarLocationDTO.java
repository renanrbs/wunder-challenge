package com.example.wunderchallenge.map;

import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

class CarLocationDTO {

    private MarkerOptions markerOptions;
    private long id;
    private String name;
    private Marker marker;

    CarLocationDTO(MarkerOptions markerOptions, Long id, String name) {
        this.markerOptions = markerOptions;
        this.id = id;
        this.name = name;
    }

    public MarkerOptions getMarkerOptions() {
        return markerOptions;
    }

    public void setMarkerOptions(MarkerOptions markerOptions) {
        this.markerOptions = markerOptions;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Marker getMarker() {
        return marker;
    }

    public void setMarker(Marker marker) {
        this.marker = marker;
    }
}
