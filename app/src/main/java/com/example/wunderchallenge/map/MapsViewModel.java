package com.example.wunderchallenge.map;

import androidx.lifecycle.ViewModel;

import com.example.wunderchallenge.BuildConfig;
import com.example.wunderchallenge.network.RetrofitFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.List;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;

public class MapsViewModel extends ViewModel {

    private Retrofit retrofit = RetrofitFactory.getRetrofit(BuildConfig.URL_CARS);

    Single<List<CarLocationDTO>> getCarLocations() {
        CarLocationsService service = retrofit.create(CarLocationsService.class);

        return service.getLocations()
                .flatMapIterable(list -> list)
                .map(this::createDto)
                .toList()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    private CarLocationDTO createDto(CarLocation carLocation) {
        LatLng position = new LatLng(carLocation.getLat(), carLocation.getLon());
        MarkerOptions markerOptions = new MarkerOptions().position(position);
        String title = carLocation.getTitle();
        if (title == null) title = "";
        return new CarLocationDTO(markerOptions, carLocation.getCarId(), title);
    }

    Boolean isMarkerClicked(Marker currentMarker) {
        if (currentMarker.getTitle() == null) return false;
        return !currentMarker.getTitle().isEmpty();
    }

    void handleMarkerFirstClick(List<CarLocationDTO> dtoList, Marker currentMarker, String placeholder) {
        for (CarLocationDTO dto : dtoList) {
            dto.getMarker().setVisible(false);
            if (dto.getMarker().getId().equals(currentMarker.getId())) {
                String name = dto.getName();
                if (name.isEmpty()) {
                    name = placeholder;
                }
                currentMarker.setTitle(name);
            }
        }

        currentMarker.setVisible(true);
    }

    Long findCurrentCarId(List<CarLocationDTO> dtoList, Marker currentMarker) {
        for (CarLocationDTO dto : dtoList) {
            if (dto.getMarker().getId().equals(currentMarker.getId())) {
                return dto.getId();
            }
        }
        return null;
    }
}
