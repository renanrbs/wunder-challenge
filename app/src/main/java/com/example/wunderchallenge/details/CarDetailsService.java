package com.example.wunderchallenge.details;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface CarDetailsService {

    @GET("cars/{carId}")
    Observable<CarDetails> getCarDetails(@Path("carId") long carId);

}
