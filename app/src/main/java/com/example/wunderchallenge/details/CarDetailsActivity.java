package com.example.wunderchallenge.details;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.BindingAdapter;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;

import com.bumptech.glide.Glide;
import com.example.wunderchallenge.R;
import com.example.wunderchallenge.databinding.ActivityCarDetailsBinding;
import com.jakewharton.rxbinding3.view.RxView;

import java.util.concurrent.TimeUnit;

import io.reactivex.disposables.CompositeDisposable;

public class CarDetailsActivity extends AppCompatActivity {

    public static String EXTRAR_CAR_ID = "extra_card_id";
    private CarDetailsViewModel viewModel;
    private CompositeDisposable disposable = new CompositeDisposable();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityCarDetailsBinding binding =
                DataBindingUtil.setContentView(this, R.layout.activity_car_details);
        long carId = getIntent().getLongExtra(EXTRAR_CAR_ID, 0);
        viewModel = new ViewModelProvider(this).get(CarDetailsViewModel.class);
        disposable.add(viewModel.getCarDetails(carId).subscribe(binding::setCar,
                error -> Toast.makeText(this, getString(R.string.default_error),
                        Toast.LENGTH_SHORT).show()));
        disposable.add(RxView.clicks(findViewById(R.id.quickRent))
                .debounce(500, TimeUnit.MILLISECONDS)
                .subscribe(click -> disposable.add(viewModel.rentCar(carId).subscribe(
                        result -> Toast.makeText(this, getString(R.string.rent_success), Toast.LENGTH_SHORT).show(),
                        error -> Toast.makeText(this, getString(R.string.rent_fail), Toast.LENGTH_SHORT).show()))));
    }

    @BindingAdapter("vehicleImage")
    public static void loadImage(ImageView view, String imageUrl) {
        Glide.with(view.getContext())
                .load(imageUrl)
                .into(view);
    }

    @Override
    protected void onDestroy() {
        disposable.dispose();
        super.onDestroy();
    }
}
