package com.example.wunderchallenge.details;

import androidx.lifecycle.ViewModel;

import com.example.wunderchallenge.BuildConfig;
import com.example.wunderchallenge.network.RetrofitFactory;
import com.example.wunderchallenge.rent.CarRent;
import com.example.wunderchallenge.rent.CarRentRequest;
import com.example.wunderchallenge.rent.RentService;

import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;

public class CarDetailsViewModel extends ViewModel {

    private Retrofit detailsRetrofit = RetrofitFactory.getRetrofit(BuildConfig.URL_CARS);
    private Retrofit rentRetrofit = RetrofitFactory.getRetrofit(BuildConfig.URL_RENT);

    Observable<CarDetails> getCarDetails(long carId) {
        CarDetailsService service = detailsRetrofit.create(CarDetailsService.class);
        return service.getCarDetails(carId)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    Single<CarRent> rentCar(long carId) {
        RentService service = rentRetrofit.create(RentService.class);
        CarRentRequest rentRequest = new CarRentRequest(carId);
        return service.rentCar(rentRequest, getToken())
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    private String getToken() {
        return "Bearer df7c313b47b7ef87c64c0f5f5cebd6086bbb0fa";
    }
}
