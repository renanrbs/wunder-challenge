package com.example.wunderchallenge.details;


public class CarDetails {

    private Long carId;
    private String title;
    private Boolean isClean;
    private Boolean isDamaged;
    private String licencePlate;
    private Integer fuelLevel;
    private Long vehicleStateId;
    private String hardwareId;
    private Long vehicleTypeId;
    private String pricingTime;
    private String pricingParking;
    private Boolean isActivatedByHardware;
    private Long locationId;
    private String address;
    private String zipCode;
    private String city;
    private Double lat;
    private Double lon;
    private Integer reservationState;
    private String damageDescription;
    private String vehicleTypeImageUrl;

    public CarDetails() {
    }

    public CarDetails(Long carId, String title, Boolean isClean, Boolean isDamaged, String licencePlate, Integer fuelLevel, Long vehicleStateId, String hardwareId, Long vehicleTypeId, String pricingTime, String pricingParking, Boolean isActivatedByHardware, Long locationId, String address, String zipCode, String city, Double lat, Double lon, Integer reservationState, String damageDescription, String vehicleTypeImageUrl) {
        this.carId = carId;
        this.title = title;
        this.isClean = isClean;
        this.isDamaged = isDamaged;
        this.licencePlate = licencePlate;
        this.fuelLevel = fuelLevel;
        this.vehicleStateId = vehicleStateId;
        this.hardwareId = hardwareId;
        this.vehicleTypeId = vehicleTypeId;
        this.pricingTime = pricingTime;
        this.pricingParking = pricingParking;
        this.isActivatedByHardware = isActivatedByHardware;
        this.locationId = locationId;
        this.address = address;
        this.zipCode = zipCode;
        this.city = city;
        this.lat = lat;
        this.lon = lon;
        this.reservationState = reservationState;
        this.damageDescription = damageDescription;
        this.vehicleTypeImageUrl = vehicleTypeImageUrl;
    }

    public Long getCarId() {
        return carId;
    }

    public void setCarId(Long carId) {
        this.carId = carId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Boolean getClean() {
        return isClean;
    }

    public void setClean(Boolean clean) {
        isClean = clean;
    }

    public Boolean getDamaged() {
        return isDamaged;
    }

    public void setDamaged(Boolean damaged) {
        isDamaged = damaged;
    }

    public String getLicencePlate() {
        return licencePlate;
    }

    public void setLicencePlate(String licencePlate) {
        this.licencePlate = licencePlate;
    }

    public Integer getFuelLevel() {
        return fuelLevel;
    }

    public void setFuelLevel(Integer fuelLevel) {
        this.fuelLevel = fuelLevel;
    }

    public Long getVehicleStateId() {
        return vehicleStateId;
    }

    public void setVehicleStateId(Long vehicleStateId) {
        this.vehicleStateId = vehicleStateId;
    }

    public String getHardwareId() {
        return hardwareId;
    }

    public void setHardwareId(String hardwareId) {
        this.hardwareId = hardwareId;
    }

    public Long getVehicleTypeId() {
        return vehicleTypeId;
    }

    public void setVehicleTypeId(Long vehicleTypeId) {
        this.vehicleTypeId = vehicleTypeId;
    }

    public String getPricingTime() {
        return pricingTime;
    }

    public void setPricingTime(String pricingTime) {
        this.pricingTime = pricingTime;
    }

    public String getPricingParking() {
        return pricingParking;
    }

    public void setPricingParking(String pricingParking) {
        this.pricingParking = pricingParking;
    }

    public Boolean getActivatedByHardware() {
        return isActivatedByHardware;
    }

    public void setActivatedByHardware(Boolean activatedByHardware) {
        isActivatedByHardware = activatedByHardware;
    }

    public Long getLocationId() {
        return locationId;
    }

    public void setLocationId(Long locationId) {
        this.locationId = locationId;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLon() {
        return lon;
    }

    public void setLon(Double lon) {
        this.lon = lon;
    }

    public Integer getReservationState() {
        return reservationState;
    }

    public void setReservationState(Integer reservationState) {
        this.reservationState = reservationState;
    }

    public String getDamageDescription() {
        return damageDescription;
    }

    public void setDamageDescription(String damageDescription) {
        this.damageDescription = damageDescription;
    }

    public String getVehicleTypeImageUrl() {
        return vehicleTypeImageUrl;
    }

    public void setVehicleTypeImageUrl(String vehicleTypeImageUrl) {
        this.vehicleTypeImageUrl = vehicleTypeImageUrl;
    }
}
