package com.example.wunderchallenge.rent;

import io.reactivex.Single;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface RentService {

    @POST("wunderfleet-recruiting-mobile-dev-quick-rental")
    Single<CarRent> rentCar(@Body CarRentRequest carRentRequest,
                            @Header("Authorization") String token);

}
