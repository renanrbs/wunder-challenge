package com.example.wunderchallenge.rent;

public class CarRentRequest {

    private long carId;

    public CarRentRequest() {
    }

    public CarRentRequest(long carId) {
        this.carId = carId;
    }

    public long getCarId() {
        return carId;
    }

    public void setCarId(long carId) {
        this.carId = carId;
    }
}
