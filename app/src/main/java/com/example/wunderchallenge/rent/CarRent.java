package com.example.wunderchallenge.rent;

public class CarRent {
    private long reservationId;
    private long carId;
    private long cost;
    private long drivenDistance;
    private String licencePlate;
    private String startAddress;
    private long userId;
    private boolean isParkModeEnabled;
    private String damageDescription;
    private String fuelCardPin;
    private Long endTime;
    private Long startTime;

    public CarRent() {
    }

    public CarRent(long reservationId, long carId, long cost, long drivenDistance, String licencePlate, String startAddress, long userId, boolean isParkModeEnabled, String damageDescription, String fuelCardPin, Long endTime, Long startTime) {
        this.reservationId = reservationId;
        this.carId = carId;
        this.cost = cost;
        this.drivenDistance = drivenDistance;
        this.licencePlate = licencePlate;
        this.startAddress = startAddress;
        this.userId = userId;
        this.isParkModeEnabled = isParkModeEnabled;
        this.damageDescription = damageDescription;
        this.fuelCardPin = fuelCardPin;
        this.endTime = endTime;
        this.startTime = startTime;
    }

    public long getReservationId() {
        return reservationId;
    }

    public void setReservationId(long reservationId) {
        this.reservationId = reservationId;
    }

    public long getCarId() {
        return carId;
    }

    public void setCarId(long carId) {
        this.carId = carId;
    }

    public long getCost() {
        return cost;
    }

    public void setCost(long cost) {
        this.cost = cost;
    }

    public long getDrivenDistance() {
        return drivenDistance;
    }

    public void setDrivenDistance(long drivenDistance) {
        this.drivenDistance = drivenDistance;
    }

    public String getLicencePlate() {
        return licencePlate;
    }

    public void setLicencePlate(String licencePlate) {
        this.licencePlate = licencePlate;
    }

    public String getStartAddress() {
        return startAddress;
    }

    public void setStartAddress(String startAddress) {
        this.startAddress = startAddress;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public boolean isParkModeEnabled() {
        return isParkModeEnabled;
    }

    public void setParkModeEnabled(boolean parkModeEnabled) {
        isParkModeEnabled = parkModeEnabled;
    }

    public String getDamageDescription() {
        return damageDescription;
    }

    public void setDamageDescription(String damageDescription) {
        this.damageDescription = damageDescription;
    }

    public String getFuelCardPin() {
        return fuelCardPin;
    }

    public void setFuelCardPin(String fuelCardPin) {
        this.fuelCardPin = fuelCardPin;
    }

    public Long getEndTime() {
        return endTime;
    }

    public void setEndTime(Long endTime) {
        this.endTime = endTime;
    }

    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }
}
