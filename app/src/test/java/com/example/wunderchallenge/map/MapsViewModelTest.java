package com.example.wunderchallenge.map;

import com.example.wunderchallenge.RxImmediateSchedulerRule;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.junit.Assert;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Single;


public class MapsViewModelTest {

    @ClassRule
    public static final RxImmediateSchedulerRule schedulers = new RxImmediateSchedulerRule();

    private MapsViewModel viewModel;

    @Before
    public void setup() {
        viewModel = new MapsViewModel();
    }

    @Test
    public void shouldGetCarLocations() {
        Single<List<CarLocationDTO>> observable = viewModel.getCarLocations();
        Assert.assertNotNull(observable);
    }

    @Test
    public void shouldMakerBeClicked() {
        Marker marker = Mockito.mock(Marker.class);
        Mockito.when(marker.getTitle()).thenReturn("test");
        Assert.assertTrue(viewModel.isMarkerClicked(marker));
    }

    @Test
    public void shouldMakerNotBeClicked() {
        Marker marker = Mockito.mock(Marker.class);
        Mockito.when(marker.getTitle()).thenReturn(null);
        Assert.assertFalse(viewModel.isMarkerClicked(marker));
        Mockito.when(marker.getTitle()).thenReturn("");
        Assert.assertFalse(viewModel.isMarkerClicked(marker));
    }

    @Test
    public void shouldHandleMarkerFirstClick() {
        Marker currentMarker = Mockito.mock(Marker.class);
        Mockito.when(currentMarker.getId()).thenReturn("1");

        Marker notClickedMarker = Mockito.mock(Marker.class);
        Mockito.when(notClickedMarker.getId()).thenReturn("2");

        String placeholder = "test";

        CarLocationDTO clickedDTO = new CarLocationDTO(new MarkerOptions(),
                1L,
                "");
        clickedDTO.setMarker(currentMarker);

        CarLocationDTO notClickedDTO = new CarLocationDTO(new MarkerOptions(),
                2L,
                "not clicked");
        notClickedDTO.setMarker(notClickedMarker);

        List<CarLocationDTO> dtoList = new ArrayList<>();
        dtoList.add(clickedDTO);
        dtoList.add(notClickedDTO);

        viewModel.handleMarkerFirstClick(dtoList, currentMarker, placeholder);

        Mockito.verify(currentMarker).setVisible(true);
        Mockito.verify(currentMarker).setTitle(placeholder);

        Mockito.verify(notClickedMarker).setVisible(false);
        Mockito.verify(notClickedMarker, Mockito.times(0)).setVisible(true);
    }

    @Test
    public void shouldFindCurrentCarId() {
        Marker currentMarker = Mockito.mock(Marker.class);
        Mockito.when(currentMarker.getId()).thenReturn("1");

        CarLocationDTO dto = new CarLocationDTO(new MarkerOptions(),
                1L,
                "");
        dto.setMarker(currentMarker);
        List<CarLocationDTO> dtoList = new ArrayList<>();
        dtoList.add(dto);

        Long result = viewModel.findCurrentCarId(dtoList, currentMarker);

        Assert.assertEquals(dto.getId(), result);
    }

    @Test
    public void shouldNotFindCurrentCarId() {
        Marker currentMarker = Mockito.mock(Marker.class);
        Mockito.when(currentMarker.getId()).thenReturn("1");

        Marker dtoMarker = Mockito.mock(Marker.class);
        Mockito.when(dtoMarker.getId()).thenReturn("2");

        CarLocationDTO dto = new CarLocationDTO(new MarkerOptions(),
                1L,
                "");
        dto.setMarker(dtoMarker);
        List<CarLocationDTO> dtoList = new ArrayList<>();
        dtoList.add(dto);

        Long result = viewModel.findCurrentCarId(dtoList, currentMarker);

        Assert.assertNull(result);
    }
}
