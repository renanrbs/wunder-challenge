package com.example.wunderchallenge.map;

import com.google.android.gms.maps.GoogleMap;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;

@RunWith(RobolectricTestRunner.class)
public class MapsActivityTest {

    private MapsActivity activity;

    @Before
    public void setup() {
        activity = Robolectric.buildActivity(MapsActivity.class).create().get();
    }

    @Test
    public void shouldCreateActivity() {
        MapsActivity activity = Robolectric.buildActivity(MapsActivity.class).create().get();
        Assert.assertNotNull(activity);
    }

    @Test
    public void shouldDestroyActivity() {
        MapsActivity activity = Robolectric.buildActivity(MapsActivity.class).create().destroy().get();
        Assert.assertNotNull(activity);
    }

    @Test
    public void shouldMapBeReady() {
        Assert.assertNull(activity.mMap);
        activity.onMapReady(Mockito.mock(GoogleMap.class));
        Assert.assertNotNull(activity.mMap);
    }

}
