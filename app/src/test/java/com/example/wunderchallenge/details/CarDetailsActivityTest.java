package com.example.wunderchallenge.details;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;

@RunWith(RobolectricTestRunner.class)
public class CarDetailsActivityTest {

    @Test
    public void shouldCreateActivity() {
        CarDetailsActivity activity = Robolectric.buildActivity(CarDetailsActivity.class).create().get();
        Assert.assertNotNull(activity);
    }

    @Test
    public void shouldDestroyActivity(){
        CarDetailsActivity activity = Robolectric.buildActivity(CarDetailsActivity.class).create().destroy().get();
        Assert.assertNotNull(activity);
    }
}
