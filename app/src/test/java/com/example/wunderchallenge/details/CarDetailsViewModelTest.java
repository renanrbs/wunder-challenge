package com.example.wunderchallenge.details;

import com.example.wunderchallenge.RxImmediateSchedulerRule;
import com.example.wunderchallenge.rent.CarRent;

import org.junit.Assert;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;

import io.reactivex.Observable;
import io.reactivex.Single;


public class CarDetailsViewModelTest {

    @ClassRule
    public static final RxImmediateSchedulerRule schedulers = new RxImmediateSchedulerRule();

    private CarDetailsViewModel viewModel;

    @Before
    public void setup() {
        viewModel = new CarDetailsViewModel();
    }

    @Test
    public void shouldGetCarDetails() {
        Observable<CarDetails> observable = viewModel.getCarDetails(1);
        Assert.assertNotNull(observable);
    }

    @Test
    public void shouldRentCar() {
        Single<CarRent> observable = viewModel.rentCar(1);
        Assert.assertNotNull(observable);
    }
}
