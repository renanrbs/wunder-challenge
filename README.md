# README #

Simple mobility app, designed according to Wunder assignment. The app
have two views:

##### Map view
Shows the user current location and the location of nearby vehicles,
clicking on a pin shows the vehicle name and hides all other vehicles.
By clicking on the pin again you are taken to the second screen.

##### Car details view
Show informations about the selected vehicle, such as licence plate 
and the vehicle image. If you'd like, you can click on the button at 
the end of the view to rent the selected vehicle.


### Technologies used ###
This assignment was developed based on the android recommended architecture
MVVM which has the benefits of easier testing and modularity.
A few technologies were used with this pattern, the ones worth noticing are:

##### Jetpack View Model
Following the MVVM pattern, the lifecycle aware view model from the jetpack
library was used. Since it handles the activity configurations changes automatically
it is easier to keep the observables states.  

##### RxJava
RxJava is a Java VM implementation of Reactive Extensions: a library 
for composing asynchronous and event-based programs by using observable 
sequences. 

It was manly used on the View Model, by exposing observables for the views
and binding their values. 

##### Data Binding
Reducing the amount of glue code that we have to write to connect the 
view and the model data. Enabling it allowed much easier writing,
without the need for the findViewById method. 

##### Mockito
Powerful mocking library, enabled writing of readable tests with less
boilerplate.

##### Roboletric
Another testing library, this library enables writing of unit tests
on a Android environment without the need of a device or emulator.

### What could have been done? 
Given the time frame, several architectures decisions were made to 
deliver on time, if more time was provided, the project probably would have
an <b>dependency injection</b> setup, this would enable writing more unit tests
and the mocking of all the services classes. Also a better UI could
be implemented, the error and succes messages are not the best they could be
and the details view does not arrange the screen in a suitable way.

Some ideas that could have been implemented, are a search for a specific
vehicle, and the overview of the car when an pin is clicked, the user 
could get an preview, based on the endpoint called on the map view.


### Who do I talk to? ###
If you have any further questions, feel free to contact me on [linkedin](https://www.linkedin.com/in/renanrbs/)
